package tasks12.task12_2_Reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Задача 12.2. Форт Нокс
 * Вы - хакер, волк-одиночка, рыцарь цифрового мира,
 * и в свободное от взлома пентагона время
 * вам пришла новая задача - разграбить федеральное хранилище,
 * избавив толстосумов от залежей желтого металла.
 * Вы должны получить все значения полей федерального хранилища,
 * сохранить эту информацию,
 * установить федеральному хранилищу для всех полей нулевые
 * или пустые значения,
 * и только после этого (!!!) создать собственное хранилище
 * (с помощью приватного конструктора
 * класса Vault) используя награбленные данные
 * в качестве параметров.
 * Т.е. по сути перенести данные из федерального хранилища в свое.
 */
public class Heist {

    public static void main(String[] args) {

        Vault federalVault = new Vault();

        Method[] methods = Vault.class.getDeclaredMethods();
        //System.out.println(Arrays.toString(methods));
        Field[] fields = Vault.class.getDeclaredFields();
        //System.out.println(Arrays.toString(fields));

        try {
            //получаем значения полей
            //сохраняем метод в переменную
            Method getDollars = federalVault.getClass().getDeclaredMethod("getDollars");
            //полученный приватный метод делаем доступным "тут, от сюда"
            getDollars.setAccessible(true);
            //вызываем полученный метод, сохраняя его результат в переменную,при этом надо указать объект на котором вызывается метод
            Object invokeDollar = getDollars.invoke(federalVault);

            Method getEuros = federalVault.getClass().getDeclaredMethod("getEuros");
            getEuros.setAccessible(true);
            Object invokeEuros = getEuros.invoke(federalVault);

            Method getTonsOfGold = federalVault.getClass().getDeclaredMethod("getTonsOfGold");
            getTonsOfGold.setAccessible(true);
            Object invokeTonsOfGold = getTonsOfGold.invoke(federalVault);

            Method getPentagonNukesCodes = federalVault.getClass().getDeclaredMethod("getPentagonNukesCodes");
            getPentagonNukesCodes.setAccessible(true);
            Object invokePentagonNukesCodes = getPentagonNukesCodes.invoke(federalVault);

            //Пример с суперметодом (см ниже)
            callMethod("getTonsOfGold",federalVault);



            //Вариант 1: меняем значения через доступ к сеттерам
            //получая метод с аргументами, надо указать тип таких аргументов, как буд-то мы прописываем шаблон такого метода (сигнатуру)
            Method setPentagonNukesCodes = federalVault.getClass().getDeclaredMethod("setPentagonNukesCodes", String.class);
            setPentagonNukesCodes.setAccessible(true);
            setPentagonNukesCodes.invoke(federalVault, "boom");

            Method setTonsOfGold = federalVault.getClass().getDeclaredMethod("setTonsOfGold", double.class);
            setTonsOfGold.setAccessible(true);
            setTonsOfGold.invoke(federalVault, 0.0);


            Method setEuros = federalVault.getClass().getDeclaredMethod("setEuros", int.class);
            setEuros.setAccessible(true);
            setEuros.invoke(federalVault, 0);

            Method setDollars = federalVault.getClass().getDeclaredMethod("setDollars", int.class);
            setDollars.setAccessible(true);
            setDollars.invoke(federalVault, 0);

            //создаем свой объект через доступ к приватному конструктору
            Constructor<? extends Vault> constructor = federalVault.getClass().getDeclaredConstructor(int.class, int.class, double.class, String.class);
            constructor.setAccessible(true);
            Vault vault = constructor.newInstance((int) invokeDollar, (int) invokeEuros, (double) invokeTonsOfGold, (String) invokePentagonNukesCodes);

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException |
                 InstantiationException e) {
            throw new RuntimeException(e);
        }

        //Пример вызова суперметодота сет (см ниже)
        callMethodSet("setDollars",federalVault,int.class,12);

        //Вариант 2: меняем значения через доступ к полям
        try {
            Field dollars = federalVault.getClass().getDeclaredField("dollars");
            dollars.setAccessible(true);
            dollars.set(federalVault, 0);
            Field euros = federalVault.getClass().getDeclaredField("euros");
            euros.setAccessible(true);
            euros.set(federalVault, 0);
            Field tonsOfGold = federalVault.getClass().getDeclaredField("tonsOfGold");
            tonsOfGold.setAccessible(true);
            tonsOfGold.set(federalVault, 0.0);
            Field pentagonNukesCodes = federalVault.getClass().getDeclaredField("pentagonNukesCodes");
            pentagonNukesCodes.setAccessible(true);
            pentagonNukesCodes.set(federalVault, null);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    //Можно добавить один суперметод для обработки поведения всех get методов выше
    public static Object callMethod(String methodName, Object target) {
        try {
            Method method = target.getClass().getDeclaredMethod(methodName);
            method.setAccessible(true);
            return method.invoke(target);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }
    //По такой же логике можно сделать и для сеттеров
    public static Object callMethodSet(String methodName, Object target, Class<?> clazz, Object args) {
        try {
            Method method = target.getClass().getDeclaredMethod(methodName, clazz);
            method.setAccessible(true);
            return method.invoke(target, args);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

}
