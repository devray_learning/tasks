package tasks12.task12_1_TDD;
import org.apache.commons.lang3.StringUtils;


public class TddPractice {
    public static String addBracers (String s){
        String string = "";
        String[] strings = s.split("\\s+");
        for (String value : strings) {
            string += "[" + value + "]";
        }
        return string;
    }

    public static String revertString (String s){
       return StringUtils.reverse(s);
    }


    public static void main(String[] args) {
        System.out.println(addBracers("пара слов"));
        System.out.println(revertString("раз"));

    }
}
