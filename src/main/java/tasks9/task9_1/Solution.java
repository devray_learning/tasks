package tasks9.task9_1;
import java.util.*;
public class Solution {

    public static void main(String[] args) {
        Human[] candidates = new Human[]{
                new Human(12, "Alex", 35),
                new Human(12, "Boris", 23),
                new Human(13, "Pavel", 17),
                new Human(143, "Klaus", 67),
                new Human(34, "Jan", 25),
                new Human(5656, "Kate", 55),
                new Human(212, "Masha", 38),
                new Human(14, "Lois", 34),
                new Human(3, "Patrik", 33),
                new Human(2, "Valentin", 32)
        };

        List<Human> listCandidates = Arrays.asList(candidates);

        List<Astronaut> astronautStream = listCandidates.stream()
                .limit(8)
                .sorted((b, c) -> c.age - b.age)
                .filter(c -> c.age > 20 && c.age < 45)
                .peek(System.out::println)
                .map(Astronaut::new)
                .sorted(Comparator.comparing(Astronaut::getName))
                .toList();
        System.out.println(astronautStream);

    }
}
