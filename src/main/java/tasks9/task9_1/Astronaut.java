package tasks9.task9_1;

import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
public class Astronaut  {

    String destinationPlanet;
    String name;
    int age;

    public Astronaut(Human human) {
        this.name = human.name;
        this.age= human.age;
    }
}
