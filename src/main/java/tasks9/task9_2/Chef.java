package tasks9.task9_2;

import java.time.LocalTime;
import java.util.*;

public class Chef {
    private final List<Package> filteredPackageList = new ArrayList<>();
    private final List<Burger> burgerList = new ArrayList<>();


    // пришедший пакет с ингридиентами фильтруем по кол-ву булок и котлет, добавляем в список только удовлетворяющие условию
    //Предполагается, что пакетов будет много, поэтому каждый корректный по составу пакет добавляется в список корректных пакетов
    public void filterPackage(Package pack) {
        int countBun = Collections.frequency(pack.getComposition(), Ingredient.BUN);
        int countCutlet = Collections.frequency(pack.getComposition(), Ingredient.CUTLET);
        if (countBun >= 2 && countCutlet >= 1) {
            filteredPackageList.add(pack);
        } else System.out.println("Отсутствует нужный игридиент в этом пакете, сделать бургер невозможно");
    }

    public void cookBurger() {
        //идем циклом по списку отфильтрованных пакетов, в которых точно есть минимум ингридиентов
        for (Package pack : filteredPackageList) {
            //создаем список для упорядовачения ингридиентов по рецепту
            List<Ingredient> localBurger = new ArrayList<>();
            //считаем сколько булочек в полученном пакете
            int countBun = Collections.frequency(pack.getComposition(), Ingredient.BUN);
            //добавляем ингридиенты, если они были в пакете
            if (pack.getComposition().contains(Ingredient.MAYONNAISE)) {
                localBurger.add(Ingredient.MAYONNAISE);
            }
            if (pack.getComposition().contains(Ingredient.CUCUMBERS)) {
                localBurger.add(Ingredient.CUCUMBERS);
            }
            if (pack.getComposition().contains(Ingredient.KETCHUP)) {
                localBurger.add(Ingredient.KETCHUP);
            }
            if (pack.getComposition().contains(Ingredient.TOMATOES)) {
                localBurger.add(Ingredient.TOMATOES);
            }
            if (pack.getComposition().contains(Ingredient.CHEESE)) {
                localBurger.add(Ingredient.CHEESE);
            }

//            почему в следующем случае будет ошибка из-за (-1)?
//            localBurger.add(Ingredient.CUTLET);
//            int indexCut = pack.getComposition().indexOf(Ingredient.CUTLET);
//            if (pack.getComposition().contains(Ingredient.CHEESE)) {
//                localBurger.add(indexCut-1, Ingredient.CHEESE);
//            }

            //кладем обязательные ингридиенты
            localBurger.add(Ingredient.CUTLET);
            localBurger.add(0, Ingredient.BUN);
            localBurger.add(localBurger.size(), Ingredient.BUN);
            //салат падает на нижнюю булку
            if (pack.getComposition().contains(Ingredient.SALAD)) {
                localBurger.add(localBurger.size() - 1, Ingredient.SALAD);
            }
            //если была третья булка, она кладется в середину бургера
            if (countBun >= 3) {
                localBurger.add(localBurger.size() / 2, Ingredient.BUN);
            }
            //кидаем бургер в список готовых бургеров
            burgerList.add(new Burger(localBurger, LocalTime.now()));
        }
    }

    //показываем готовые бургеры из списка
    public void showBurgers() {
        for (Burger b : burgerList) {
            System.out.println(b.toString());
        }
    }
}










