package tasks9.task9_2;
import java.util.List;


public class Solution {
    public static void main(String[] args)  {
        Chef chef = new Chef();

        chef.filterPackage(
                new Package(List.of(Ingredient.CUTLET, Ingredient.BUN,
                        Ingredient.SALAD, Ingredient.CHEESE))
        );

        chef.filterPackage(
                new Package(List.of(Ingredient.CUTLET, Ingredient.BUN, Ingredient.BUN, Ingredient.BUN,
                        Ingredient.SALAD, Ingredient.CHEESE, Ingredient.CUCUMBERS, Ingredient.MAYONNAISE,
                        Ingredient.TOMATOES, Ingredient.KETCHUP))
        );
        chef.filterPackage(
                new Package(List.of(Ingredient.CUTLET,  Ingredient.BUN, Ingredient.BUN,
                        Ingredient.SALAD, Ingredient.TOMATOES))
        );


        chef.cookBurger();
        chef.showBurgers();
    }
}
