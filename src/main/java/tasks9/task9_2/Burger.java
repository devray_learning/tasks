package tasks9.task9_2;

import java.time.LocalTime;
import java.util.List;

public class Burger {

    private final List<Ingredient> burgerIngredients;
    LocalTime time;
    public Burger(List<Ingredient> burgerIngredients, LocalTime time) {
        this.burgerIngredients = burgerIngredients;
        this.time=time;
    }


    @Override
    public String toString() {
        return "Burger {"+
                "Состав: " + burgerIngredients +
                ", Время изготовления - " + time +
                '}';
    }
}
