package tasks9.task9_2;

import java.util.List;

public class Package {

    private final List<Ingredient> composition;

    public Package(List<Ingredient> composition) {
        this.composition = composition;
    }

    public List<Ingredient> getComposition() {
        return composition;
    }

}
