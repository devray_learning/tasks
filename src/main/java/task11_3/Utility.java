package task11_3;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Utility {


    //ПРОВЕРКА EMAIL
    public static class InccorectEmailException extends RuntimeException {
        public InccorectEmailException(String message) {
            super(message);
        }
    }

    public static boolean patternMatches(String emailAddress, String regexPattern) {
        return Pattern.compile(regexPattern)
                .matcher(emailAddress)
                .matches();
    }

    public void checkEmailAddress(String email) throws InccorectEmailException {

        String defaultPattern = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\\\.[A-Za-z0-9-]+)*(\\\\.[A-Za-z]{2,})$";

        if (patternMatches(email, defaultPattern)) {
            System.out.println("Проверено!");
        } else throw new InccorectEmailException("НЕКОРРЕКТНЫЙ EMAIL");
    }

    public void checkEmailAddress(String email, String regExPattern) throws InccorectEmailException {

        if (patternMatches(email, regExPattern)) {
            System.out.println("Проверено!");
        } else throw new InccorectEmailException("НЕКОРРЕКТНЫЙ EMAIL");
    }

    //ПРОВЕРКА ЧЕТНОСТИ
    public boolean checkParity(Integer i) {
        return i % 2 == 0;
    }

    //ФИЛЬТР СПИСКА СТРОК
    public List<String> sortList(List<String> list) {
        return list.stream()
                .filter(c -> c.length() <= 6 || c.startsWith("а")).collect(Collectors.toList());
    }


    public static void main(String[] args) throws InccorectEmailException {
        Utility utility = new Utility();

        System.out.println(utility.checkParity(4));

        for (String s : utility.sortList(Arrays.asList("sada", "абвлыадц", "фывл3д34лвыа", "авыв"))) {
            System.out.println(s);
        }


        //result is true
        utility.checkEmailAddress("username@domain.com", "^(.+)@(.+)$");

        //result is false (throws exception)
        utility.checkEmailAddress("username@domain.com");


    }


}
