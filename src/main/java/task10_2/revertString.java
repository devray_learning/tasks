package task10_2;
import org.apache.commons.lang3.StringUtils;
import java.util.List;


public class revertString {
    public static void reversAndSwapCasePhrase(List<String> list) {

        for (String s: list) {
            System.out.println(StringUtils.swapCase(StringUtils.reverse(s)));
        }

    }

    public static void main(String[] args) {
        reversAndSwapCasePhrase(List.of("Довод", "пРОВЕРКА", "Перевернуть любую Строку"));
    }
}
