package task15_1_RestApi.pojo_breeds;


import lombok.Data;

@Data
public class LinksItem{
	private boolean active;
	private String label;
	private Object url;
}
