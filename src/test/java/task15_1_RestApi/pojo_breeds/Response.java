package task15_1_RestApi.pojo_breeds;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data

public class Response {
    //Необходимо вручную "переименовать" названия переменных для десериализатора таким образом,
    //как они называются в JSON файле. Аннотация JsonProperty позволяет это сделать
    @JsonProperty("per_page")
    private int perPage;
    private List<DataItem> data;
    @JsonProperty("last_page")
    private int lastPage;
    @JsonProperty("next_page_url")
    private String nextPageUrl;
    @JsonProperty("prev_page_url")
    private Object prevPageUrl;
    @JsonProperty("first_page_url")
    private String firstPageUrl;
    private String path;
    private int total;
    @JsonProperty("last_page_url")
    private String lastPageUrl;
    private int from;
    private List<LinksItem> links;
    private int to;
    @JsonProperty("current_page")
    private int currentPage;
}