package task15_1_RestApi.pojo_breeds;
import lombok.Data;

@Data
public class DataItem{
	private String country;
	private String coat;
	private String origin;
	private String pattern;
	private String breed;


	public DataItem() {
	}
}
