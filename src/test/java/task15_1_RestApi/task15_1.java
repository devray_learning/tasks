package task15_1_RestApi;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.response.ResponseBodyExtractionOptions;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.annotations.Test;
import task15_1_RestApi.pojo_breeds.DataItem;
import task15_1_RestApi.pojo_breeds.LinksItem;
import task15_1_RestApi.pojo_breeds.Response;
import task15_1_RestApi.pojo_facts.Fact;

import java.util.List;

import static io.restassured.RestAssured.given;

public class task15_1 {

    RequestSpecification requestSpecBuilder = new RequestSpecBuilder()
            .setBaseUri("https://catfact.ninja/#/Breeds/getBreeds")
            .setContentType(ContentType.JSON)
            .log(LogDetail.ALL)
            .build();

    ResponseSpecification responseSpecification = new ResponseSpecBuilder()
            .log(LogDetail.BODY)
            .expectContentType(ContentType.JSON)
            .expectStatusCode(200)
            .build();


    @Test(description = "№1 Проверка значения поля по JsonPath")
    public void testCheckValueOfField() {
        given()
                .spec(requestSpecBuilder)
                .basePath("/breeds")
                .when()
                .get()
                .then()
                .spec(responseSpecification)
                .body("data[0].breed", Matchers.equalTo("Abyssinian"));
    }

    @Test(description = "№2 Проверка структуры JSON и присутствия полей")
    public void testStructureAndPresenceOfElements() {

        ResponseBodyExtractionOptions body =
                given().baseUri("https://catfact.ninja/#/Breeds/getBreeds")
                        .basePath("/breeds") //путь эндпоинта относительно baseUri пути
                        .contentType(ContentType.JSON)
                        .when()
                        .get()
                        .then()
                        .spec(responseSpecification)
                        .extract().body();

// чтобы "забрать" весь JSON файл указываем пустой JsonPath - "" или "$" как путь к root
        Response response = body.jsonPath().getObject("", Response.class);
        List<DataItem> data = body.jsonPath().getList("data", DataItem.class);
        LinksItem link = body.jsonPath().getObject("links[2]", LinksItem.class);
        String country = body.jsonPath().get("data[1].country");
    }

    @Test(description = "№3 Проверка не нулевого значения поля")
    public void testNotNullValue() {
        given()
                .spec(requestSpecBuilder)
                .basePath("/fact")
                .when()
                .get()
                .then()
                .spec(responseSpecification)
                .body("fact", Matchers.notNullValue());
        // Matchers.typeCompatibleWith(String.class) - можно ли сделать проверку (сразу) на выдаваемый тип значения?
        // Или надо сохранять в стринг переменную для этого
    }

    @Test(description = "№4 Проверка работы алгоритма расчетов длины строки")
    public void testStringLength() {
        Fact fact = given()
                .spec(requestSpecBuilder)
                .basePath("/fact")
                .when()
                .get()
                .then()
                .spec(responseSpecification)
                //забираем объект, берем у него длину и проверяем тут вручную выданную длину и реальную длину поля стринг
                .extract().body().jsonPath().getObject("", Fact.class);

        Assert.assertEquals(fact.getLength(), fact.getFact().length());
    }

    @Test(description = "№5 Проверка времени отклика")
    public void testResponseTime() {
        given()
                .spec(requestSpecBuilder)
                .basePath("/facts")
                .when()
                .get()
                .then()
                .spec(responseSpecification)
                .time(Matchers.lessThan(2000L));

    }

    @Test(description = "№6 Проверка значения соединения в хэдере на keep-alive")
    public void testConnection() {
        given()
                .spec(requestSpecBuilder)
                .basePath("/facts")
                .when()
                .get()
                .then()
                .spec(responseSpecification)
                .header("Connection", "keep-alive");
    }
}
