package task15_1_RestApi.pojo_facts;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Fact {

	@JsonProperty("fact")
	private String fact;

	@JsonProperty("length")
	private int length;

}