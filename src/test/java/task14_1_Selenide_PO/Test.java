package task14_1_Selenide_PO;

public class Test implements PageObjectSupplier {
   //имплементируем интефейс и вызываем его дефолтные методы, которые создают обхекты пейджев (необходимо для вызова методов пейджев)
   @org.testng.annotations.Test(description = "LitresTest")
    public void litresTest () {
       searchPage().searchItem("грокаем алгоритмы");
       pickPage().pickFirstItem();
       checkPage().checkPrices();
       checkPage().checkAddButton();
   }
   //Замечание: тесты литрес и резонит следовало бы разнести по разным классам

   //Замечание 2: паттерн Fluent / Chain of invocation, но тут его тоже можно было бы применить и код текста для длинных цепочек вызовов мог бы читаться по-бодрее. Основное правило которое в этом паттерне рекомендую практиковать - методы, выполнение которых не приводит к смене страницы - должны возвращать тип той же страницы и делать return this. методы, выполнение которых приводит к смене страницы - имеют возвращаемый тип void.

   @org.testng.annotations.Test(description = "Rezonit_Negative")
   public void rezonitNegative () {
      selectPage().selectItem();
      parametrPage().setParameters("10","-999","");
      checkRezonitPage().checkWarning();
   }

   @org.testng.annotations.Test(description = "Rezonit_Positive")
   public void positiveNegative () {
      selectPage().selectItem();
      parametrPage().setParameters("10","20","20");
      parametrPage().calculate();
      checkRezonitPage().checkTotalPrice();
   }



}
