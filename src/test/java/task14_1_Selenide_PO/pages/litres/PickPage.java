package task14_1_Selenide_PO.pages.litres;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class PickPage {
    private final SelenideElement firstItem = $x("//img[contains(@alt,'Грокаем алгоритмы')]");

    public void pickFirstItem() {
        firstItem.click();
    }
}
