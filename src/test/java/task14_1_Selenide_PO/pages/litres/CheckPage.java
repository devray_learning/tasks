package task14_1_Selenide_PO.pages.litres;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class CheckPage {

    private final SelenideElement firstPrice = $x("//strong[contains(text(),'399')]");
    private final SelenideElement secondPrice = $x("//meta[contains(@content,'599')]//following-sibling::div/strong");
    private final SelenideElement addItem = $x("//div[contains(text(),'Добавить')]");

    public void checkPrices() {
        firstPrice.shouldBe(exist, visible);
        secondPrice.shouldBe(exist, visible);
    }
    public void checkAddButton() {
        addItem.shouldBe(exist, visible);
    }

}
