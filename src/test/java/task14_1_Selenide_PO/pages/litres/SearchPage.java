package task14_1_Selenide_PO.pages.litres;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.open;

public class SearchPage {
    private final SelenideElement searchLine = $x("//*[@id=\"__next\"]/div[1]/header/div[2]/div[2]/div/div/form/div/input");
    private final SelenideElement searchButton = $x("//button[text()='Найти']");

    public void searchItem(String itemName) {
        open("https://www.litres.ru/");
        searchLine.sendKeys(itemName);
        searchButton.click();

    }

}
