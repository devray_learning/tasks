package task14_1_Selenide_PO.pages.rezonit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.open;

public class SelectPage {

    SelenideElement platy = $x
            ("//ul[@class='d-none d-lg-flex justify-content-between main-menu']//a[contains(text(),'Срочные печатные платы')]");

        public void selectItem () {
            open("https://www.rezonit.ru/");
            platy.click();
        }
}
