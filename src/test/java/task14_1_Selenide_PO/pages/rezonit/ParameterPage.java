package task14_1_Selenide_PO.pages.rezonit;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.$x;

public class ParameterPage {

    SelenideElement insertLength = $x("//input[@id='calculator-input-1']");

    SelenideElement insertWight = $x("//input[@id='calculator-input-2']");

    SelenideElement amountOfItem = $x("//input[@id='calculator-input-3']");

    SelenideElement calculate = $x("//button[@id='calculate']");

    //собираем все вводимые данные в один метод с параметрами для удобства
    public void setParameters(String length, String wight, String amountOfItems) {
        insertLength.sendKeys(length);
        insertWight.sendKeys(wight);
        amountOfItem.sendKeys(amountOfItems);
    }
    //Замечание:мНеобходимость описывать методы на мелкие действия все еще остается. Поэтому самое идеальное в твоем случае - описать методы setLength, setWidth, setAmount и вызвать из своего метода setParametrs - вместо прямого взаимодействия с полями. Так на перспективу у нас окажется максимально гибкий ассортимент методов в PageObject. Да, и я бы названию метода setParameters добавил конкретики - большо уж он абстрактно звучит, например сделать setParameters -> setCircuitBoardParameters. Если наши методы будут называться "совершитьДействие", "передатьДанные", "произвестиРасчет" - ух не позавидую я тому кто будет с нуля с этим кодом разбираться.

    public void calculate () {
        calculate.click();
    }



}
