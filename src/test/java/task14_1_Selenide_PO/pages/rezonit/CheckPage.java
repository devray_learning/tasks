package task14_1_Selenide_PO.pages.rezonit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$x;

public class CheckPage {
    SelenideElement redWarning = $x("//div[@class='alert alert-danger']");
    SelenideElement totalPrice = $x("//span[@id='total-price']");


    public void checkWarning() {
        redWarning.should(exist);
    }

    public void checkTotalPrice () {
        totalPrice.should(exist);
    }
}
