package task14_1_Selenide_PO;

import task14_1_Selenide_PO.pages.litres.CheckPage;
import task14_1_Selenide_PO.pages.litres.PickPage;
import task14_1_Selenide_PO.pages.litres.SearchPage;
import task14_1_Selenide_PO.pages.rezonit.ParameterPage;
import task14_1_Selenide_PO.pages.rezonit.SelectPage;

public interface PageObjectSupplier {

    //Т.к. для вызова методов указанных в наших пейджах понадобится создавать их обхекты, то мы делаем интрефейс с дефолтными методами на создание обхектов пейджев и просто имплементирует этот интерефейс в наще классе вызова тестов. Тогда у этого касса будут эти методы интерфейса и из можно будет вызывать, а не создавать там обхекты пейджев.
    default SearchPage searchPage() {
        return new SearchPage();
    }

    default CheckPage checkPage() {
        return new CheckPage();

    }

    default PickPage pickPage() {
        return new PickPage();
    }

    default ParameterPage parametrPage() {
        return new ParameterPage();
    }

    default SelectPage selectPage() {
        return new SelectPage();
    }

    default task14_1_Selenide_PO.pages.rezonit.CheckPage checkRezonitPage() {
        return new task14_1_Selenide_PO.pages.rezonit.CheckPage();
    }



}
