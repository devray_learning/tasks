package task11_3_test;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import task11_3.Utility;
import java.util.Arrays;
import java.util.List;
import static org.mockito.Mockito.*;

public class TestUtility {
    Utility utility;


    public TestUtility() {
    }

    @BeforeClass
    public Utility makeUtility() {
        return utility = new Utility();
    }

    @DataProvider(
            name = "DP"
    )
    public static Object[][] dataBB() {
        return new Object[][]{{"username@domain.com", "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\\\.[A-Za-z0-9-]+)*(\\\\.[A-Za-z]{2,})$"}, {"username@domain.com", "^(.+)@(\\\\S+)$"}};
    }

    @Test(
            dataProvider = "DP"
    )
    public void testPatternMatches(String emailAddress, String regexPattern) {
        Assert.assertFalse(Utility.patternMatches(emailAddress, regexPattern));
    }

//        @Test(description = "тест должен упасть, если исключение не выпадает")
//    public void testCheckEmailAddressWithPatternThrows() {
//        //первым аргументом идет класс ожидаемого исключения, вторым лямбда на метод вызывающий исключение
//        Assert.assertThrows(Utility.InccorectEmailException.class,
//                () -> utility.checkEmailAddress("username@domain.com", "^(.+)@(.+)$"));
//
//    }
    @Test
    void testCheckEmailAddressWithPattern() {
        Utility u = mock(Utility.class);

        doNothing().when(u).checkEmailAddress(isA(String.class), isA(String.class));
        u.checkEmailAddress("username@domain.com", "^(.+)@(.+)$");

        verify(u, times(1)).checkEmailAddress("username@domain.com", "^(.+)@(.+)$");
    }

    @Test
    public void testCheckEmailAddress() {
        //первым аргументом идет класс ожидаемого исключения, вторым лямбда на метод вызывающий исключение
        Assert.assertThrows(Utility.InccorectEmailException.class,
                () -> utility.checkEmailAddress("username@domain.com"));

    }


    @Test
    public void testUtilityObjectForCheckParity() {
        Assert.assertNotNull(utility);
    }

    @Test
    public void testCheckParity() {
        boolean expectedResult = false;
        boolean actualResult = utility.checkParity(3);
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testSortList1() {
        List<String> actualList = utility.sortList(Arrays.asList("sada", "абвлыадц", "фывл3д34лвыа", "авыв"));
        Assert.assertListContainsObject(actualList, "авыв", "объект присутствует");
    }

    @Test
    public void testSortList2() {
        List<String> actualList = utility.sortList(Arrays.asList("sada", "абвлыадц", "фывл3д34лвыа", "авыв"));
        List<String> expectedList = List.of("sada", "абвлыадц", "авыв");
        Assert.assertEquals(expectedList, actualList);
    }

    @Test
    public void testSortList3() {
        List<String> actualList = utility.sortList(Arrays.asList("sada", "абвлыадц", "фывл3д34лвыа", "авыв"));
        List<String> expectedList = List.of("sada", "абвлыадц", "авыв");
        Assert.assertNotSame(expectedList,actualList);
    }
}

