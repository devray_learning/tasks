package task12_1_test;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import tasks12.task12_1_TDD.TddPractice;

public class TestTdd {
    @DataProvider(
            name = "DP"
    )
    public static Object[][] dataBB() {
        return new Object[][]{{"[пара][слов]"}, {"[не][три][слова]"}};
    }

    @Test(groups = "SimpleTest")
    public void testAddBracers() {
    String expectedResult = "[пара][слов]";
    String actualResult = TddPractice.addBracers("пара слов");
    Assert.assertEquals(expectedResult,actualResult);
    }

    //Замечание: тест не тестирует функционал утилиты(!)
    @Test (groups = "MediumTest", dataProvider = "DP", dependsOnMethods = "testAddBracers", alwaysRun = true)
    public void testAddBracers_if(String s) {
        Assert.assertTrue(s.contains("["));

    }

    @Test(groups = "SimpleTest")
    public void testRevertString(){
        Assert.assertEquals(TddPractice.revertString("раз"),"зар");
        Assert.assertEquals(TddPractice.revertString("я устал придумывать примеры"),"ыремирп ьтавымудирп латсу я");
    }

    @Test(groups = "HardTest", dependsOnGroups = "SimpleTest", alwaysRun = true)
    public void testRevertString_trow(){
       Assert.assertNotNull(TddPractice.revertString("llun"));
    }


}
