package task13_Selenide.task13_2;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.open;

public class Negative_Rezonit {

    public static void main(String[] args) {
        open("https://www.rezonit.ru/");
        SelenideElement platy = $x
                ("//ul[@class='d-none d-lg-flex justify-content-between main-menu']//a[contains(text(),'Срочные печатные платы')]");
        platy.click();

        SelenideElement insertLength = $x("//input[@id='calculator-input-1']");
        insertLength.sendKeys("10");

        SelenideElement insertWight = $x("//input[@id='calculator-input-2']");
        insertWight.sendKeys("-999");

        SelenideElement redWarning = $x("//div[@class='alert alert-danger']");
        redWarning.should(exist);
    }
}
