package task13_Selenide.task13_2;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.open;

public class Positive_Rezonit {
    public static void main(String[] args) {
        open("https://www.rezonit.ru/");
        SelenideElement platy = $x
                //Почти никакой способ не позволяет нажать на эту ссылку, вылетает с
                // Exception in thread "main" Element should be clickable: interactable and enabled
                //("//a[contains(text(),'Срочные печатные')] ");
                //("/html/body/div[2]/header/div[1]/div/div/div[1]/div/div[4]/ul/li[1]/a");
                //$(withText("Срочные печатные платы"));
                //только этот xPath, полученный через SelectorHub помог
                        //Замечание: Можно сделать так, укоротив предложенный тобой локатор:
                        //div[@class='header-content']//a[contains(text(), 'Срочные печатные')]
                       //Если разобраться в этом локаторе, то мы по сути говорим - найди мне в хедере (верхней менюхе) ссылку, у которой текст содержит слова "Срочные платы".
                        ("//ul[@class='d-none d-lg-flex justify-content-between main-menu']//a[contains(text(),'Срочные печатные платы')]");
        platy.click();

        SelenideElement insertLength = $x("//input[@id='calculator-input-1']");
        insertLength.sendKeys("10");

        SelenideElement insertWight = $x("//input[@id='calculator-input-2']");
        insertWight.sendKeys("20");

        SelenideElement amountOfItem = $x("//input[@id='calculator-input-3']");
        amountOfItem.sendKeys("20");

        SelenideElement calculate = $x("//button[@id='calculate']");
        calculate.click();

        SelenideElement totalPrice = $x("//span[@id='total-price']");
        totalPrice.should(exist);






    }
}
