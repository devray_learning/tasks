package task13_Selenide.task13_1;

import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.open;

public class task13_1_Litres {
    //Локаторы - лучше использовать @data-test-id. На разных проектах аттрибуты для автотестов могут называться по-разному - иногда @selenium-data, иногда @test-id, иногда @data-test-id.

    //Ввести в поисковую строку запрос
    SelenideElement searchLine = $x
            //пришлось взять автоXpath,
            // т.к по моему тегу элемент правильно находился и работал в дебаге,
            // но не всегда работал при запуске ( //input[contains(@placeholder,'Литрес')] )
                    //Замечание: Тут вместо такого локатора можно было набросать альтернативу: //input[@data-test-id='header__search-input--desktop'] - она должна работать стабильно, заодно и цепляется за упомянутый @data-test-id.
                    ("//*[@id=\"__next\"]/div[1]/header/div[2]/div[2]/div/div/form/div/input");

    //Кликнуть по кнопке "Найти"
            //Замечание: Тут локатор неплохой, и его даже можно оставить при условии что сайт не имеет мультиязычности, т.е. не доступен например еще на английском и китайском. Иначе как понимаешь, кнопка "найти" будет 100% называться по-другому в других языках. Альтернатива здесь - тот же самый @data-test-id: //button[@data-test-id='header__search-button--desktop']
    SelenideElement searchButton = $x
            ("//button[text()='Найти']");

    //Кликнуть по первому товару в поисковой выдаче
    SelenideElement firstItem = $x
            ("//img[contains(@alt,'Грокаем алгоритмы')]");

    //Проверить что на странице товара присутствует первая цена (по абонементу)
    SelenideElement firstPrice = $x
            ("//strong[contains(text(),'399')]");
    //Проверить что на странице товара присутствует вторая цена (на скачивание)
    SelenideElement secondPrice = $x
            ("//meta[contains(@content,'599')]//following-sibling::div/strong");

    //Проверить что на странице товара присутствует кнопка "Добавить в корзину"
    SelenideElement addItem = $x
            ("//div[contains(text(),'Добавить')]");

    @Test
    //метод нестатический, поэтому из него можно вынести заданные переменные в класс и оставить только вызов методов на них
    public void test() {
        open("https://www.litres.ru/");
        searchLine.sendKeys("грокаем алгоритмы");
        searchButton.click();
        firstItem.click();
        firstPrice.shouldBe(exist, visible);
        secondPrice.shouldBe(exist, visible);
        addItem.shouldBe(exist, visible);
    }


}
