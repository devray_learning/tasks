package task16_1_RestApi_p2.pojos;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@lombok.Data
@AllArgsConstructor
@NoArgsConstructor
public class Hotel {
    public String currency;
    public int eapid;
    public String locale;
    public int siteId;
    public String propertyId;
}
