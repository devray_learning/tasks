package task16_1_RestApi_p2.pojos;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor //почему-то не захотел работать без этой аннотации, нужен явный дефолтный конструктор, в задании 15.1 работало и без него
public class Coordinate {

	@JsonProperty("lat")
	private String lat;

	@JsonProperty("long")
	private String jsonMemberLong;

}