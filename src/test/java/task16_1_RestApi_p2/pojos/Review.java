package task16_1_RestApi_p2.pojos;


import lombok.AllArgsConstructor;
import lombok.Data;



@Data
@AllArgsConstructor
public class Review {

        public String currency;
        public int eapid;
        public String locale;
        public int siteId;
        public String propertyId;
        public int size;
        public int startingIndex;


}
