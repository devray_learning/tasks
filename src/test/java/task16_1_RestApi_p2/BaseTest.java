package task16_1_RestApi_p2;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.hamcrest.Matchers;

public class BaseTest {
    public static RequestSpecification requestSpecification() {

        return new RequestSpecBuilder()
                .log(LogDetail.ALL)
                .setBaseUri("https://rapidapi.com/apidojo/api/hotels4")
                .setContentType(ContentType.JSON)
                .build();
    }

    public static RequestSpecification requestSpecificationWithAuthorization() {

        return new RequestSpecBuilder()
                .log(LogDetail.ALL)
                .setBaseUri("https://hotels4.p.rapidapi.com")
                .setContentType(ContentType.JSON)
                .addHeader("X-RapidAPI-Key", "59292f1f46msh78f76552227350ap1d0b3ejsn1b3c6c58c765")
                .addHeader("X-RapidAPI-Host", "hotels4.p.rapidapi.com")
                .build();
    }

    public static ResponseSpecification responseSpecification() {

        return new ResponseSpecBuilder()
                .log(LogDetail.STATUS)
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200)
                .expectResponseTime(Matchers.lessThan(20000L))
                .build();
    }

}
