package task16_1_RestApi_p2;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import task16_1_RestApi_p2.pojos.Coordinate;
import task16_1_RestApi_p2.pojos.Hotel;
import task16_1_RestApi_p2.pojos.Review;

import java.time.LocalDate;
import java.util.List;


public class ApiTest extends BaseTest {

    @Test(description = "№1 Проверка значения поля  x-rapidapi-region из заголовка ответа и статус кода", groups = "getTests")
    public void testRegion() {

        RestAssured
                .given()
                .spec(requestSpecificationWithAuthorization())
                .basePath("/locations/v3/search")
                .when()
                .get()
                .then()
                .statusCode(204)
                .header("x-rapidapi-region", "AWS - eu-central-1");
    }


    @Test(description = "№2 Проверка зачений Coordinates для заданного города", groups = "getTests")
    public void testCoordinates() {

        Coordinate expectedItem = new Coordinate("40.712843", "-74.005966");

        Coordinate actualItem =
                RestAssured
                        .given()
                        .spec(requestSpecificationWithAuthorization())
                        .basePath("/locations/v3/search")
                        .param("q", "new york") // - так можно устанавливать значения полей в запросе. (по задаче - смотреть обязательные параметры для формирования запроса на сайте апи)
                        .when()
                        .get()
                        .then()
                        .spec(responseSpecification())
                        .extract().jsonPath().getObject("sr[0].coordinates", Coordinate.class);

        Assert.assertEquals(actualItem, expectedItem);
    }

    @Test(description = "№3 Проверка поля name по и работы выдачи детальных данных по передаваемому объекту", groups = "postTests")
    public void testDetails() {

        Hotel hotel = new Hotel("USD", 1, "en_US", 300000001, "9209612");

        RestAssured
                .given()
                .spec(requestSpecificationWithAuthorization())
                //.basePath("/properties/v2/detail")
                .body(hotel)
                .when()
                .post("properties/v2/detail") //endpoint можно указывать и сюда
                .then()
                .spec(responseSpecification())
                .body("data.propertyInfo.summary.name", Matchers.equalTo("Golden Time Hotel"));
    }


    //Не думаю, что тут я взял подходящие условия для такого теста (просто хотелось опробовать методики), т.к. у москвы могут быть координаты NY (по ошибке), а и те и другие содержатся в листе, т.е. тест пройдет, хотя коордитаны некорректные. Максимум, что пришло в голову вынести объекты координат в поименованные переменные, может если как-то сделать сверку с наименованием переменной координат, тогда будет более-менее...
    @DataProvider(name = "DP")
    public static Object[][] dataBB() {
        return new Object[][]{{"new york"}, {"moscow"}};
    }

    @Test(description = "№4 Проверка зачений Coordinates для заданных в DP городов", groups = "getTests", dataProvider = "DP")
    public void testCoordinatesMulti(String s) {
        Coordinate ny = new Coordinate("40.712843", "-74.005966");
        Coordinate msc = new Coordinate("46.731651", "-117.001343");
        List<Coordinate> coordinateList = List.of(ny, msc);

        Coordinate coordinate =
                RestAssured
                        .given()
                        .spec(requestSpecificationWithAuthorization())
                        .basePath("/locations/v3/search")
                        .param("q", s)
                        .get()
                        .then()
                        .spec(responseSpecification())
                        .extract().jsonPath().getObject("sr[0].coordinates", Coordinate.class);

        Assert.assertListContainsObject(coordinateList, coordinate, s);
    }

    @Test(description = "№5 Проверка получения поля отзыва", groups = "postTests")
    public void test() {

        Review review = new Review("USD", 1, "en_US", 300000001, "9209612", 10, 0);

        String s =
                RestAssured
                        .given()
                        .spec(requestSpecificationWithAuthorization())
                        .body(review)
                        .when()
                        .post("/reviews/v3/list")
                        .then()
                        .spec(responseSpecification())
                        .extract().jsonPath().get("data.propertyInfo.reviewInfo.reviews[0].text");
        System.out.println(s);
    }

    //Подобный тест уже был, но тут исползуется функционал faker.
    @Test(description = "№6 Проверка выдачи значения по полю shortName", groups = "getTests")
    public void testCountry() {
        Faker faker = new Faker();
        String s =
                RestAssured
                        .given()
                        .spec(requestSpecificationWithAuthorization())
                        .basePath("/locations/v3/search")
                        .param("q", faker.country().capital())
                        .when()
                        .get()
                        .then()
                        .spec(responseSpecification())
                        .extract().jsonPath().get("sr[0].regionNames.shortName");
        System.out.println(s);
    }

    @Test(description = "№7 Проверка зачений productToken по id. Конвертация в новый id.", groups = "getTests")
    public void testId() {
                RestAssured
                        .given()
                        .spec(requestSpecificationWithAuthorization())
                        .basePath("/properties/v2/resolve-url")
                        .param("id", "ho546370")
                        .when()
                        .get()
                        .then()
                        .spec(responseSpecification())
                        .body("product.productToken",Matchers.equalTo("12325893"));
    }

    @Test(description = "№8 Проверка длины поля content-lenght заголовка", groups = "postTests")
    public void testContentLength() {

        Hotel hotel = new Hotel("USD", 1, "en_US", 300000001, "9209612");

        RestAssured
                .given()
                .spec(requestSpecificationWithAuthorization())
                .body(hotel)
                .when()
                .post("properties/v2/get-content")
                .then()
                .spec(responseSpecification())
                .header("content-length","25727");
    }

    @Test(description = "№9 Проверка длины поля content-lenght заголовка", groups = "postTests")
    public void testContentLength1() {

        Hotel hotel = new Hotel("USD", 1, "en_US", 300000001, "9209612");

        RestAssured
                .given()
                .spec(requestSpecificationWithAuthorization())
                .body(hotel)
                .when()
                .post("properties/v2/get-content")
                .then()
                .spec(responseSpecification())
                .header("date",Matchers.not(LocalDate.now()));
    }

    @Test(description = "№10 Проверка статус кода", groups = "getTests")
    public void testRegionStatusCode() {
        RestAssured
                .given()
                .spec(requestSpecificationWithAuthorization())
                .basePath("/locations/v3/search")
                .when()
                .get()
                .then()
                .statusCode(204);
    }



}